/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "stdlib.h"

#define FALSE  0
#define TRUE   1
#define TRANSMIT_BUFFER_SIZE  32

#define CAPACITY 50

typedef struct inArray_t {
    uint8 cap;
    uint8 len;
    uint32 array[CAPACITY];
} inArray;

uint32 firFilter(uint32* inArray, float* coefArray, uint8 len) {
    float y = 0.0;
    for(uint8 i = 0; i<len; i++) {
        y = y + (coefArray[i] * inArray[i]);
    }
    y = y / len;
    return (uint32)y;
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    //uint32 Output;
    //uint8 Ch;
    //uint8 single = FALSE;
    
    inArray inputVals;
    inputVals.cap = 50;
    inputVals.len = 0;
    float coef[10] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
    uint32 outVal = 0;
    for(uint8 i = 0; i< CAPACITY; i++) {
        inputVals.array[i] = 0;
    }
    
    char TransmitBuffer[TRANSMIT_BUFFER_SIZE];
    
    ADC_Start();
    UART_Start();
    
    ADC_StartConvert();
    
    UART_PutString("Start\n\r\n\r");
    
    for(uint8 i = 0; i < CAPACITY; i++) {
        while(!(ADC_IsEndConversion(ADC_RETURN_STATUS)));
        if(ADC_IsEndConversion(ADC_RETURN_STATUS)) {
           inputVals.array[i] = ADC_CountsTo_mVolts(ADC_GetResult16());
           inputVals.len++; 
        }
    }
    
    /*
    for(uint8 i = 0; i < CAPACITY; i++) {
        sprintf(TransmitBuffer, "%ld mV\r\n", inputVals.array[i]);
        UART_PutString(TransmitBuffer);
    }*/
    //for(uint8 i = 0;i < 20;i++)
    for(;;)
    {
        while(!(ADC_IsEndConversion(ADC_RETURN_STATUS)));
        if(ADC_IsEndConversion(ADC_RETURN_STATUS)) {
            inputVals.len ++;
            if(inputVals.len > inputVals.cap) {
                uint32 tmpArray[CAPACITY] = { 0 };
                for(uint8 i = 0; i<(inputVals.cap-1); i++) {
                    tmpArray[i+1] = inputVals.array[i];
                }
                
                tmpArray[0] = ADC_CountsTo_mVolts(ADC_GetResult32());
                //sprintf(TransmitBuffer, "Input: %ld mV\r\n", tmpArray[0]);
                //UART_PutString(TransmitBuffer);
                //UART_PutString("\n\r\n\r");
                memcpy(inputVals.array, tmpArray, sizeof(inputVals.array));
                inputVals.len = inputVals.cap;
            }
        }
        outVal = firFilter(inputVals.array, coef, inputVals.len);
        sprintf(TransmitBuffer, "%ld\r\n", outVal);
        UART_PutString(TransmitBuffer);
    }
}

/* [] END OF FILE */
